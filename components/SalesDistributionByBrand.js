import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'

export default function SalesDistributionByBrand({ rawData }){

	// carData = array of obj [
	// 	{
	// 		brand: "Dodge",
	// 		sales: 47
	// 	}
	// ]

	const [brands, setBrands] = useState([])
	const [sales, setSales] = useState([])
	const [salesDate, setSalesDate] = useState([])
	const [bgColors, setBgColors] = useState([])

	useEffect(() => {

        let makes = []
        rawData.forEach(element => {
            //current obj doesnt exist,  save 
            if(!makes.find(make => make === element.make)){
              makes.push(element.make)
            }
        })
        console.log(makes)
        setBrands(makes)

    }, [rawData])

    useEffect(() => {
        setSales(brands.map(brand => {

            let sales = 0

            rawData.forEach(element => {
                if(element.make === brand){
                    sales = sales + parseInt(element.sales)
                }
            })

            return sales
        }))

        setBgColors(brands.map(() => `#${colorRandomizer()}`))
    }, [brands])

	const data = {
		labels: brands, 
		datasets: [{
			data: sales,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors
		}]
	}

	return (
		<React.Fragment>
			<Pie data={data}/>
		</React.Fragment>
	)
}